# Templates

Templates for the final pitching

# How to Use
1. Download the template immediately on the first day, to backup your results and make the presentation easier
2. Upload the presentation on the folder provided to your team by the Hackathon organizers.
2. Use the template on the Pitching session, 29.06.2022.

# License
Templates are provided in the context of the EU-IoT Hackathon under a Creative Commons License.
